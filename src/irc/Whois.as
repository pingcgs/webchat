package irc
{
	import mx.controls.Alert;

	public class Whois
	{
		import contenedores.HashMap;

		public static var listaWhois:HashMap = new HashMap();		
		
		public var ident:String;
		
		public var canales:Array;
		
		public var usaChatea:Boolean = false;
		
		public var away:String = null;
		
		public var fin:Boolean = false;
		
		public function Whois(s:String){
			if(!listaWhois.containsKey(s.toLowerCase())) listaWhois.put(s.toLowerCase(),this);
			Usr.ircp.WriteRawMessage("WHOIS "+ s.toLowerCase());
		}
		
		public function setIdent(s:String):void {
			this.ident = s;
			if(ident == Config.ident || ident == "fl_chatea") usaChatea=true;
		}

		public function setFin():void {
			this.fin = true;
		}
		
		public function setAway(s:String):void {
			this.away = s;
		}
		
		public function setCanales(arr:Array):void {
			var ar:Array = arr.join(" ").split(" ");
			this.canales = ar;
		}
		
		public static function tieneWhois(s:String):Boolean {
			return ((listaWhois.getValue(s.toLowerCase())as Whois).fin);
		}
		
		public static function getWhois(s:String):Whois {
			return (listaWhois.getValue(s.toLowerCase()) as Whois);
		}
		
	}
}