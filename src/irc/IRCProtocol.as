package irc
{
	
	import events.ConnectEvent;
	import events.ConnectedEvent;
	import events.RecvMessageEvent;
	
	import flash.net.Socket;
	import flash.system.Security;
	import flash.utils.*;
	import flash.events.ProgressEvent;
	
	import mx.controls.Alert;
	import mx.core.FlexGlobals;
	import mx.utils.StringUtil;
	
	public class IRCProtocol extends Socket
	{
		/** The port we're connecting on */
		private var port:uint;
	
		/** The host we're connecting to */
		public var host:String;

		/** The host we're connecting to */
		public var nick:String;
		
		/** The buffer for received messages */
		private var buffer:String;	
				
		private var input: String;
		
		private var charset:String = "utf-8";
		
		
		private var reg:RegExp = new RegExp("(.+)!(.+)@(.+)");
					
		/**
		* Constructor, creates a new RFB socket connection to a host
		* on a specific port.
		*/
		public function IRCProtocol( host:String, port:uint, nick:String){
			super( host, port );
			this.host = host;
			this.port = port;
			this.nick = nick;
			buffer="";
			this.charset = Config.charset;
			this.addEventListener(ProgressEvent.SOCKET_DATA, this.onRead);
		}
			
		private function onRead(event:ProgressEvent) : void
		{
			var data:Array;
			var length:int;
			var i:int;
			var event:* = event;
			try
			{
				data = this.readMultiByte(this.bytesAvailable, this.charset).split("\r\n");
			}
			catch (error:Error)
			{
			}
			if (data != null)
			{
				length = data.length;
				i;
				while (i < length)
				{
					
					this.input = this.input + (data[i] as String);
					if (this.input.length > 1 && i < (length - 1))
					{
						decodeMessage(this.input );
						this.input = "";
					}
					i = (i + 1);
				}
			}
			return;
		}// end function
		
		

		
		/**
		* Register nickname
		*/
		public function RegisterNick():void {
			if(Config.password != null && Config.password != "") {
				WriteRawMessage("PASS " + Config.password);
			}
			
			WriteRawMessage("NICK " + nick);
			WriteRawMessage("USER " + Config.ident + " host server :" +Config.realname);
			flush();
		}		
	/*
		public function ReadMessage():void {
			while ( bytesAvailable > 0 )
			{
				var byte: uint = readUnsignedByte();
				var next: uint;
				
				if ( byte == 13 )
				{
					if ( bytesAvailable >= 1 )
					{
						next = readUnsignedByte();
						
						if ( next == 10 )
						{
							trace( '<-', input );
							decodeMessage(input );
							input = '';
						}
						else
						{
							input += String.fromCharCode( byte );
							input += String.fromCharCode( next );
						}
					}
					else
						input += String.fromCharCode( byte );
				}
				else
					input += String.fromCharCode( byte );
			}
			decodeMessage(input );
		}
*/
		
		private function decodeMessage(mess:String):void
		{
			
				mess = StringUtil.trim(mess);
				trace("< " + mess);
				if(mess == "") return;
				//auto reply pong
				if(mess.substr(0,6) == "PING :") 
				{
					WriteRawMessage("PONG " + mess.substr(6,mess.length-6));
				}
				
				
				/*
			     * From RFC 1459:
				 *  <message>  ::= [':' <prefix> <SPACE> ] <command> <params> <crlf>
				 *  <prefix>   ::= <servername> | <nick> [ '!' <user> ] [ '@' <host> ]
				 *  <command>  ::= <letter> { <letter> } | <number> <number> <number>
				 *  <SPACE>    ::= ' ' { ' ' }
				 *  <params>   ::= <SPACE> [ ':' <trailing> | <middle> <params> ]
				 *  <middle>   ::= <Any *non-empty* sequence of octets not including SPACE
				 *                 or NUL or CR or LF, the first of which may not be ':'>
				 *  <trailing> ::= <Any, possibly *empty*, sequence of octets not including
				 *                   NUL or CR or LF>
				*/
				
				
				//parse Previx
				if(mess.charAt(0) == ':') {
					var re1:RegExp = new RegExp("[ @!]");
					var prefix:String = mess.split(re1)[0].substring(1);
					var ident = mess.replace(reg,"$2");
					mess = mess.substring(mess.indexOf(" ")+1);
				}
				
				//parse commande / code
				var cmd:String;
			 	cmd = mess.split(" ",1)[0];
			 	mess = mess.substring(mess.indexOf(" ")+1);
			 	if(cmd == "004") dispatchEvent(new ConnectedEvent());
			 
			 	//parse params
			 	var params:Array;
				if(mess.indexOf(":") > -1) {
			 		params = mess.split(":")[0].split(" ")
					if(params[params.length-1] == "")
			 		params[params.length-1] = mess.substring(mess.indexOf(":")+1);
			 		else params = params.concat(mess.substring(mess.indexOf(":")+1));
				}
				else {
					params = mess.split(" ");
				}
				var me:RecvMessageEvent = new RecvMessageEvent(prefix,cmd,params,ident);
				dispatchEvent(me);	
		}
		
		
		public function WriteRawMessage(output:String):void{
			trace( '->', output );

				this.writeMultiByte(output + "\r\n", this.charset);
				this.flush();

		}
		
		/**
		 * Send a message to the server
		 */
		public function WriteMessage(chanID:String, mess:String):void
		{
			var mess2Serv:String = "";
			mess = StringUtil.trim(mess);
			
			//Commands
			if(mess.charAt(0) == '/')
			{
				var tok:Array = mess.split(' ');
				var cmd:String = tok[0].toLowerCase();
				switch(cmd) {
					case "/j" :
					case "/join" :
						join(tok.slice(1).join(" "));
						return;	
					case "/part" :	
						mess2Serv = "PART ";
						mess2Serv += tok.slice(1).join(" ");
						break;
					case "/msg" :	
						mess2Serv += "PRIVMSG " + tok[1] + " :" + tok.slice(2).join(" ");
						break;
					
					case "/query":	
					case "/q":	
						Usr.abrirPrivado(tok.slice(1).join(" "));
						break;									
					case "/quit" :	
						mess2Serv = "QUIT ";
						mess2Serv += tok.slice(1).join(" ");
						break; 
					case "/notice" :	
						mess2Serv = "NOTICE ";
						mess2Serv += tok.slice(1).join(" ");
						break;
					case "/me" :	
						mess2Serv = "PRIVMSG " + chanID + " :\x01ACTION";
						mess2Serv += mess.substring(cmd.length);
						mess2Serv += "\x01";
						break;
					case "/op" :	
						mess2Serv = "MODE " + chanID + " +o ";
						if(tok[1]) mess2Serv += tok[1];
						else return;
						break;
					case "/voice" :	
						mess2Serv = "MODE " + chanID + " +v ";
						if(tok[1]) mess2Serv += tok[1];
						else return;
						break;
					case "/deop" :	
						mess2Serv = "MODE " + chanID + " -o ";
						if(tok[1]) mess2Serv += tok[1];
						else return;
						break;
					case "/devoice" :	
						mess2Serv = "MODE " + chanID + " -v ";
						if(tok[1]) mess2Serv += tok[1];
						else return;
						break;
					case "/ban" :	
						mess2Serv = "MODE " + chanID + " +b ";
						if(tok[1]) mess2Serv += tok[1];
						else return;
						break;
					case "/unban" :	
						mess2Serv = "MODE " + chanID + " -b ";
						if(tok[1]) mess2Serv += tok[1];
						else return;
						break;
					case "/list":
						FlexGlobals.topLevelApplication.moduloPop("ListCanales","pop","Lista de salas");
						break;
					case "/kick" :	
						mess2Serv = "KICK " + chanID + " ";
						if(tok[1]) mess2Serv += tok[1];
						else return;
						break;		
					default :
						WriteRawMessage(mess.substr(1));
						return;
					break;
				}
			}
			else mess2Serv += "PRIVMSG " + chanID + " :"  + mess;
			trace("> " + mess2Serv);
			WriteRawMessage(mess2Serv);			
		}
		
		
		public function cambiarNick(n:String,s:String = ""):void {
			if(s == "") WriteRawMessage("NICK " + n)
			else WriteRawMessage("LOGIN " + n + " " + s);
		}
		
		

		public function join(chanID:String):void {
			try {
			var temp:String = StringUtil.trim(chanID);
			var arrCanales:Array = temp.split(",");
			var sEntrada:String = "";
				for(var i:int=0; i <arrCanales.length; i++) {
						var temp2:String = arrCanales[i];
						temp = temp2.split("%23").join("");
						if(temp2.charAt(0) != "#") {
							temp2 = "#" + temp2;
						}
						if(i == 0) sEntrada = temp2;
						else sEntrada += ","+ temp2;
				}
				WriteRawMessage("JOIN " +	sEntrada);
				WriteRawMessage("MODE " +	sEntrada);
			 
			}catch(er:Error) {

			}
		}


	}
}
