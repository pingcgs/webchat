package contenedores
{
	public class HashMapEntry
	{
		protected var _key: * ;
		
		protected var _value: * ;
		
		public function HashMapEntry(key:*, value:*)
		{
			this._key   = key;
			this._value = value;
		}
		
		public function set key(key:*) : void
		{
			_key = key;
		}
		
		public function get key() : *
		{
			return _key;
		}
		
		public function set value(value:*) : void
		{
			_value = value;
		}
		
		public function get value() : *
		{
			return _value;
		}
	}

		
}
