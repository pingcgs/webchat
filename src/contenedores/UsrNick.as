package contenedores
{
	public class UsrNick
	{
		public var mNick:String;
		public var webcam:Boolean = false;
		public var foto:Boolean = false;
		public var facebook:Boolean = false;
		public var conectado:Boolean = false;
		public var votos:int = 0;
		
		
		public function UsrNick(nick:String,f:Boolean= false,w:Boolean= false,fb:Boolean = false,vot:int = 0){
			mNick = nick;
			this.webcam = w;
			this.foto = f;
			this.facebook = fb;
			this.votos = vot;
		}
		
		public function tieneImagen():Boolean {
			return this.foto;
		}
		public function tieneWebcam():Boolean {
			return this.webcam;
		}
		
		
	}
}