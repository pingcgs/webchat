package events 
{

import flash.events.Event;

/**
 * A MessageEvent is used to signal that a message is received.
 */
public class RecvMessageEvent extends Event
{
	/** Static constant for the event type to avoid typos with strings */
	public static const RECV_MESS_EVENT_TYPE:String = "RecvMessageEvent";

	/** The message's prefix */
	public var prefix:String;
	
	/** The message's command */
	public var cmd:String;		
	
	/** The ident message */
	public var ident:String;
	
	/** The message's destination */
	public var params:Array;	


			
	
	/**
	 * Constructor, create a new ConnectEvent with a specific host and port
	 */
	public function RecvMessageEvent( prefix:String = "",cmd:String = "",params:Array = null,ident = null)
	{
		super( RECV_MESS_EVENT_TYPE );
		this.prefix = prefix;
		this.cmd = cmd;
		this.params = params;
		this.ident = ident;
	}

} // end class
} // end package
