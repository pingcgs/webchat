package modulos
{
	public interface ICamPub
	{
		function desconectar():void;
		function conectar(s:String):void;
		function estoyConectado():Boolean;
	}
}