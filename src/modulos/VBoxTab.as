package modulosMgrs
{
	import mx.containers.VBox;
	
	public class VBoxTab extends VBox
	{
		public var chanId:String;
		
		public function VBoxTab(id:String)
		{
			super();
			chanId = "Tab " + id;
			this.label = id;
		}
	}
}