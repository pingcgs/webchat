package modulos 
{
	
	import flash.events.Event;
	
	import mx.modules.IModuleInfo;
	import mx.modules.Module;


	public class ModuloEvent extends Event
	{
		public var mod:Modulo;
		public var modulo:Module;
		public var contenedor:Object = null;
		public var parametros:Object = null;
		
		public function ModuloEvent(EVENTO:String = "preparado",info:Modulo  = null)
		{
			super(EVENTO);
			this.mod = info;
		}
		
	} // end class
} // end package
