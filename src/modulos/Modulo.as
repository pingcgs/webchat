package modulos
{
	
import contenedores.HashMap;

import flash.display.DisplayObject;
import flash.events.EventDispatcher;

import mx.controls.Alert;
import mx.core.mx_internal;
import mx.events.ModuleEvent;
import mx.modules.IModuleInfo;
import mx.modules.Module;
import mx.modules.ModuleManager;
import mx.utils.object_proxy;

import org.flexunit.internals.namespaces.classInternal;

	
public class Modulo extends  EventDispatcher {
	
		
		public static var MODULO_PREPARADO:String = "moduloPreparado";		
		public var info:IModuleInfo;
		public var modulo:Object;
		public var nombre:String;
		public var titulo:String;
		public var url:String;
		
		public var contenedor:Object;
		public var parametros:Object;
		
		public var enviarAlTop:Boolean = false;
		public var ico:Class = null;
		
		/**
		 * Tipo: 
		 * 		pop 	-> Ventana popup modal
		 * 		tab 	-> Pestaña
		 * 		float	-> Flotante
		 */ 
		public var tipo:String;
		
		public function Modulo(url:String,nuevo:Boolean = false):void {
			this.url = url;
			info = ModuleManager.getModule("modulos/"+url+".swf");
			info.addEventListener(ModuleEvent.READY, modEventHandler);
			info.addEventListener(ModuleEvent.ERROR,errorModule);
		}
		
		public function errorModule(e:ModuleEvent):void {
			Alert.show("Error "+ e.errorText);
		}
		
		public function descargar():void {
			info.unload();
		}
		
		public function cargar():void {
			info.load();
		}
		
		private function modEventHandler(e:ModuleEvent):void {
			var evRed:ModuloEvent = new ModuloEvent(Modulo.MODULO_PREPARADO,this);
			var n:String = null;
			evRed.mod = this;
			modulo = info.factory.create();
			if(this.nombre == null) n = this.url;
			else n = this.nombre;
			mx.core.FlexGlobals.topLevelApplication.listaModulos.put(n,this);
			mx.core.FlexGlobals.topLevelApplication.modulosCargados.put(url,info);
			if(enviarAlTop) mx.core.FlexGlobals.topLevelApplication.dispatchEvent(evRed);
				else dispatchEvent(evRed);
		}
	}
}