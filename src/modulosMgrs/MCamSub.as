package modulosMgrs
{
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	
	import modulos.ICamPub;
	
	import mx.controls.Alert;
	import mx.core.UIComponent;
	import mx.events.ModuleEvent;
	import mx.managers.CursorManager;
	import mx.managers.PopUpManager;
	import mx.modules.ModuleLoader;
	
	public class MCamSub extends ModulosMgr
	{
		public static var IT:ICamPub = null;
		public var nick:String;
		
		override public function MCamSub(nombreModulo:String,ancho:int = 0, alto:int = 0,pop:Boolean = true,cont:UIComponent = null,nick2:String=null)
		{
			super(nombreModulo,ancho,alto,pop,cont);
			this.nick = nick2;
		}

		
	 	override public  function IniciarModulo(e:ModuleEvent):void {
			CursorManager.removeBusyCursor();
			var modCarga:ModuleLoader =  listaModulos.getValue(e.target.url);
			if(modCarga != null) {
				if( modCarga.child== null) {
					ErrorModuloHijo();
				}else {
					IT =  modCarga.child as ICamPub;
					
					if(popUp) {
						modCarga.width = ancho;
						modCarga.height = alto;
						PopUpManager.addPopUp(modCarga, este as DisplayObject , true);				
						PopUpManager.centerPopUp(modCarga);
					} else {
						modCarga.percentWidth = 100;
						modCarga.percentHeight = 100;
						contenedor.addChild(modCarga);
					}
					MCamSub.IT.conectar(nick);
				} 	
			} else  ErrorModuloNulo();
		}		
		
		

	}
}