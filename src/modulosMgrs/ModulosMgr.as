package modulosMgrs
{
	
	import contenedores.HashMap;
	
	import flash.display.DisplayObject;
	
	
	import mx.controls.Alert;
	import mx.core.UIComponent;
	import mx.events.ModuleEvent;
	import mx.managers.CursorManager;
	import mx.managers.PopUpManager;
	import mx.modules.ModuleLoader;

	
	public class ModulosMgr
	{

		public static var listaModulos:HashMap = new HashMap();
		public static var este:Object = null;
		public var ancho:int = 400;
		public var alto:int = 400;
		public var popUp:Boolean = true;
		public var contenedor:UIComponent;
	
		public function ModulosMgr (nombreModulo:String,ancho:int = 0, alto:int = 0,pop:Boolean = true,cont:UIComponent = null) {
			if(ancho!=0) this.ancho = ancho;
			if(ancho!=0) this.alto = alto;
			this.popUp = pop;
			contenedor = cont;
			CursorManager.setBusyCursor();
			if(listaModulos.containsKey(nombreModulo)) {
				ErrorModuloIniciado(); 	
			} else {
				var modCarga:ModuleLoader = new ModuleLoader;
				var url:String = "modulos/" + nombreModulo + ".swf";
				modCarga.url= url;
				modCarga.loadModule();
				modCarga.addEventListener(ModuleEvent.READY,IniciarModulo);
				listaModulos.put(url,modCarga);
			}
		}
		
		public static function finalizarModulo(nombreModulo:String):void {
			var url:String = "modulos/" + nombreModulo + ".swf";
			var modCarga:ModuleLoader =  listaModulos.getValue(url);
			if(modCarga == null) {
				ErrorFinalizarModulo();
			}else {
				PopUpManager.removePopUp(modCarga);
				modCarga.unloadModule();	
			} 
		}
		
		public  function IniciarModulo(e:ModuleEvent):void {
			CursorManager.removeBusyCursor();
			var modCarga:ModuleLoader =  listaModulos.getValue(e.target.url);
			if(modCarga != null) {
				if( modCarga.child== null) {
					ErrorModuloHijo();
				}else {
					if(popUp) {
						modCarga.width = ancho;
						modCarga.height = alto;
						PopUpManager.addPopUp(modCarga, este as DisplayObject , true);				
						PopUpManager.centerPopUp(modCarga);
					} else {
						modCarga.percentWidth = 100;
						modCarga.percentHeight = 100;
						contenedor.addChild(modCarga);	
					}
				} 	
			} else  ErrorModuloNulo();
		}
		
		
		/**
		 * 	Control de Errores
		 **/
		
		public function ErrorModuloNulo():void {
			Alert.show("Error al Iniciar: Modulo Vacío");
		}		
		
		public function ErrorModuloHijo():void {
			Alert.show("Error al Iniciar el Módulo");
		}
		
		public function ErrorModuloIniciado():void {
			Alert.show("Módulo Ya Iniciado");
		}


		public static function ErrorFinalizarModulo():void {
			Alert.show("Error al Finalizar Módulo");
		}
	
	}
}