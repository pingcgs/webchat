package 
{	
	import flash.net.SharedObject;
	
	import modulos.Modulo;
	import modulos.ModuloEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.AdvancedDataGrid;
	import mx.controls.Alert;
	import mx.core.FlexGlobals;
	import mx.events.ModuleEvent;
	
	import utilidades.Media;
		
	
	public class Config
	{
		//parametros conexion irc
		public static var servidor:String = "webchat.chatsfree.net";
		public static var puerto:int = 6667;
		
		public static var servidorError:Array = ["webflash.chateagratis.net:6667", "webflash2.chatsfree.net:6667"];
		
		public static var servidorAlternativo = "maracuya.chatsfree.net";
		public static var puertoError:int = 443;
		
		public static var realname:String = "CHAT V3 Chateagratis.net";
		public static var password:String = "";
		
		
		public static var nickEntrada:String = "chatea";
		public static var canalEntrada:String = "chateagratis.net"; 
		
		public static var tamLetra:int = 13;

		public static var verHora:Boolean = false;
		public static var verMiniaturas:Boolean = false;
		public static var verTopic:Boolean = false;
		public static var verProtegida:Boolean = false;
		
		public static var eventEmitterFuncion:String = "launchEvent";
		
		public static var usarSQL:Boolean = false;
		
		public static var charset:String = "utf-8";
		
		public static var token:String = "";
		
		public static var serverSQL:String = "http://flash.chateagratis.net"
			
		public static var css:String = "../css/chateagratis.css";
		public static var webCorp:String = "www.chateagratis.net";
		
		public static var ident:String = "fl_cgs000";
		
		public static var webcamServer:String = "rtmp://5.135.189.93/oflaDemo/";
		public static var email:String = "ant@antweb.es";
		
		public static var verAvatar:Boolean = true;
		public static var retardoConexion:int = 10000;
		public static var verAppFacebook:Boolean = false;
		public static var showUserPopups:Boolean = true;
		public static var urlFacebook:String = "http://www.facebook.com/chateagratis";
		
		public static var urlReplace:String ="";
		public static var urlSearch:String = "";
		
		public static var tamAvatar:int = 19;
		
		/*Ident Extend*/
		public static var web:String = "fl_"; //chateagratis
		public static var sexo:String = "0"; //Sin definir
		public static var avatar:String = "00"; //Ninguno

		
		public static function cambiarLetra():void {
			for each(var c:Object in FlexGlobals.topLevelApplication.tabs.getChildren()) {
				try {
					c.ircView.cambiarLetra();
					
				}catch (e:Error) {
					
				}
			}
		}
		
		public static function getBoolean(parametro):Boolean {
			if((typeof parametro) == "boolean") return parametro;
			else {
				var tempValue:Boolean = parametro == "true"; // returns true: this is what I suggested
				var boolValue:Boolean = tempValue ? true : false;
				return boolValue;
			}
		}
		
		public static function cargarParametros(parametros:Object):void {
			
			Usr.cache= SharedObject.getLocal( "chatea.cache" );
			
			if(parametros.show_avatars != null) Config.verAvatar = Config.getBoolean(parametros.show_avatars);
	
			if(parametros.vertopic != null) Config.verTopic = Config.getBoolean(parametros.vertopic);

			if(parametros.verProtegida != null) Config.verProtegida = Config.getBoolean(parametros.verProtegida);
			
			if(parametros.showUserPopups != null) Config.showUserPopups = Config.getBoolean(parametros.showUserPopups);

			if(parametros.verAppFacebook != null)  Config.verAppFacebook =  Config.getBoolean(parametros.verAppFacebook);
			
			if(parametros.realname != null && parametros.realname != "")  Config.realname = parametros.realname;

			if(parametros.password != null && parametros.password != "")  Config.password =  parametros.password;

			if(parametros.token != null && parametros.token != "")  Config.token =  parametros.token;
			
			if(parametros.email != null && parametros.email != "")  Config.email =  parametros.email;
			
			if(parametros.charset != null && parametros.charset != "")  Config.charset =  parametros.charset;
			
			if(parametros.urlSearch != null && parametros.urlSearch != "")  Config.urlSearch =  parametros.urlSearch;
			if(parametros.urlReplace != null && parametros.urlReplace != "")  Config.urlReplace =  parametros.urlReplace;
			
			if(parametros.retardoConexion != null && parametros.retardoConexion != "")  Config.retardoConexion =  parametros.retardoConexion;

			
			if(parametros.server != null && parametros.server != ""){
				Config.servidor = parametros.server;
			}
			
			if(parametros.servidorAlternativo != null && parametros.servidorAlternativo != ""){
				Config.servidorAlternativo = parametros.servidorAlternativo;
			}
			
			if(parametros.ident != null && parametros.ident != "") {
				Config.ident = parametros.ident;
			}
			
			if(Media.esIdentExtendida(Config.ident)) {
				Config.web = Config.ident.substr(3,3);
				Config.sexo = Config.ident.substr(6,1);
				Config.avatar = Config.ident.substr(7,Config.ident.length);
			}
			
			if(parametros.urlFacebook != null) {
				 Config.urlFacebook = "http://"+ parametros.urlFacebook;
			}
			

						
			if(parametros.nick == null || parametros.nick == "") {
				var numAleat:Number = Math.floor(Math.random()* 999999);
				Config.nickEntrada = "chatea" + numAleat;
			} else {
				Config.nickEntrada = parametros.nick;
			}
			
			if(parametros.channels != null && parametros.channels != "") {
				Config.canalEntrada = parametros.channels;	
			}
			
			if(parametros.css != null && parametros.css != "") {
				Config.css = parametros.css;	
			}
			
			if(parametros.web != null && parametros.web != "") {
				Config.webCorp = parametros.web;	
			}
			

		}
	
	
	}	
	
}