package utilidades
{
	public class Icons
	{
		
		/*Iconos List*/			
		
		[Embed(source="../data/iconos/salas-chat.png")]
		public static var salasChat:Class;		
				
		[Embed(source="../data/iconos/salas-chat2.png")]
		public static var salasChat2:Class;		
		
		[Embed(source="../data/iconos/privado.png")]
		public static var privado:Class;		

		[Embed(source="../data/iconos/privado2.png")]
		public static var privado2:Class;

		[Embed(source="../data/iconos/op.gif")]
		public static var usrOp:Class;
		
		[Embed(source="../data/iconos/usuario.gif")]
		public static var usrDef:Class;
		
		[Embed(source="../data/iconos/camara.png")]
		public static var usrFoto:Class;

		[Embed(source="../data/letraAumentar.png")]
		public static var letraAumentar:Class;
				
		[Embed(source="../data/letraReducir.png")]
		public static var letraReducir:Class;
	
		[Embed(source="../data/iconos/settings.png")]
		public static var herramientas:Class;		

		[Embed(source="../data/iconos/icowebcam.png")]
		public static var webcam:Class;		
		
		[Embed(source="../data/iconos/salacam.png")]
		public static var salacam:Class;		

		[Embed(source="../data/iconos/webcamonwarning.png")]
		public static var webcamWarn:Class;	
		
		[Embed(source="../data/iconos/icowebcamblock.png")]
		public static var webcamBlock:Class;			
		
		[Embed(source="../data/iconos/casa.png")]
		public static var iconoHome:Class;
		
		[Embed(source="../data/iconos/iconoTrans.png")]
		public static var iconoTrans:Class;
		
		[Embed(source="../data/iconos/add.png")]
		public static var amigo:Class;
		
		[Embed(source="../data/iconos/forbidden.png")]
		public static var bloqueado:Class;		
		
		/*EmotIconos
		[Embed(source="../data/img/angry.png")]
		public static var angry:Class;

		[Embed(source="../data/img/aw.png")]
		public static var aw:Class;
		
		[Embed(source="../data/img/beer.png")]
		public static var beer:Class;
		
		[Embed(source="../data/img/bigsmile.png")]
		public static var bigsmile:Class;
		
		[Embed(source="../data/img/cash.png")]
		public static var cash:Class;
		
		[Embed(source="../data/img/cool.png")]
		public static var cool:Class;		
		
		[Embed(source="../data/img/crazy.png")]
		public static var crazy:Class;
		
		[Embed(source="../data/img/crown.png")]
		public static var crown:Class;

		[Embed(source="../data/img/evil.png")]
		public static var evil:Class;
		
		[Embed(source="../data/img/ghost.png")]
		public static var ghost:Class;
		
		[Embed(source="../data/img/glasses.png")]
		public static var glasses:Class;
		
		[Embed(source="../data/img/innocent.png")]
		public static var innocent:Class;
		
		[Embed(source="../data/img/love.png")]
		public static var love:Class;
		
		[Embed(source="../data/img/mail.png")]
		public static var mail:Class;		
		
		[Embed(source="../data/img/lol.png")]
		public static var lol:Class;
		
		[Embed(source="../data/img/party.png")]
		public static var party:Class;
		
		[Embed(source="../data/img/quest.png")]
		public static var quest:Class;
		
		[Embed(source="../data/img/sad.png")]
		public static var sad:Class;
		
		[Embed(source="../data/img/santa.png")]
		public static var santa:Class;

		[Embed(source="../data/img/shock.png")]
		public static var shock:Class;
		
		[Embed(source="../data/img/wink.png")]
		public static var wink:Class;
		
		[Embed(source="../data/img/tongue.png")]
		public static var tongue:Class;
		
		[Embed(source="../data/img/corazon.png")]
		public static var corazon:Class;
		
		[Embed(source="../data/img/rosa.png")]
		public static var rosa:Class;
		
		[Embed(source="../data/img/oups.png")]
		public static var oups:Class;
		
		*/
		public static var smilies:Array =    new Array(':enfado:',      ':(',  ':cerveza:',      ':-D',    ':dinero:',  '8-)',  'S-)',  '3:-)',  '}:-)',  '(:O)',     '8-)',    'O:-)', ':love:', ':mail:', ':lol:',   'c:-)',   ':pregunta:', ':_(', ':santa:',   ':-O',  ';-)',    ':-P',     ':c:',  '@--',  ':-x');
		public static var smiliesGfx:Array = new Array(   'angry',     'aw', 'beer', 'bigsmile',   'cash', 'cool', 'crazy', 'crown', 'evil',
			'ghost', 'glasses', 'innocent',   'love',   'mail',   'lol', 'party', 'quest', 'sad',   'santa', 'shock',
			'wink', 'tongue', 'corazon', 'rosa', 'oups');
		/*EmotIcons*/

		
	}
}