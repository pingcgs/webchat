package utilidades
{
	import flash.display.DisplayObject;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import modulos.CamSub;
	import modulos.Modulo;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;

	public class Media
	{
		import contenedores.HashMap;
		import contenedores.Usuarios;
		
		import flash.events.Event;
		import flash.net.URLLoader;
		import flash.net.URLRequest;
		import flash.net.URLRequestMethod;
		import mx.core.FlexGlobals;
		import flash.external.ExternalInterface;
		
		import contenedores.UsrNick;
		
		
		public static var usadaWebcam:Boolean = false;
	
		
		public static var CAM_WIDTH:int = 180;
		public static var CAM_HEIGHT:int = 120;
		
		public static var fanFb:Boolean = false;
		public static var radioOn:Boolean = false;
		public static var webcamOn:Boolean = false;
		public static var mediaSala:String = null;
		public static var numCams:int = 0;
		public static var foto:Boolean = false;
		public static var promocionCam:Boolean = false;
		public static var urlFoto:String = "";
		public static var camPop:Boolean = false;
		
		private static var urlUsuarios:String = "http://flash.chateagratis.net/a/med.php";
		public static var XmlUsuarios:XML;
		
		public static var bloqueos:HashMap = new HashMap();
		public static var usuariosCam:HashMap = new HashMap();
		public static var salasCam:HashMap = new HashMap();
		
		public static var camsTotal:int = 1;
		
		//Carga el XML
		public static function CargarXMLWebcam():void {
			//Creo el objeto cargador
			var loader:URLLoader = new URLLoader();
			//Le añado los listeners
			loader.addEventListener(Event.COMPLETE, completarUsuarios);
			//Creo el objeto que contendrá la petición
			var peticion:URLRequest = new URLRequest(urlUsuarios);
			//Cargo la petición
			loader.load(peticion);
			
		}	
		
		public static function esIdentExtendida(ident:String):Boolean {
			var patron:RegExp = /^fl_(.*){3}([0-2])([a-zA-Z0-9]){1,2}$/;
			return patron.test(ident);
		}
		
		public static function obtenerAvatar(ident:String):String {
			var av:String = ident.substr(7,Config.ident.length);
			if(Media.esIdentExtendida(ident)) {
				if(av == "00") return ("../data/avatar/00.png");
				return ("../data/avatar/" +av+".png");
			} else return ("../data/avatar/00.png");
		}
		

		public static function obtenerWeb(ident:String):String {
			return ident.substr(3,3);
			
		}			
		
		public static function obtenerSexo(ident:String):String {
			return ident.substr(6,1);
		}		
		
		public static function obtenerColorSexo(ident:String):Object{
			
			switch (ident.substr(6,1)) {
				case "1": return 0x0000FF;
				case "2": return 0xFF1493;
				default: return	0x000000;
			}
		}	
		
		public static function iniciarSQL():void {
			if(!Config.usarSQL) {
				Config.usarSQL = true;
				var loader:URLLoader = new URLLoader();
				var request:URLRequest = new URLRequest(Config.serverSQL + "/a/addLogOnline.php?p="+Usr.ircp.nick+"&i="+Config.ident);
				loader.load(request);
			}
		}
		
		public static function ponerFoto():void {
			Config.usarSQL = true;
			var m:Modulo = FlexGlobals.topLevelApplication.listaModulos.getValue("Imagenes");
			if(m != null) { 
			m.modulo.actualizar();
			}
			Media.usadaWebcam = true;
			var loader:URLLoader = new URLLoader();
			var request:URLRequest = new URLRequest(Config.serverSQL + "/a/addfoto.php?nombre="+ Usr.ircp.nick);
			loader.load(request);
			Media.urlFoto = Media.resolverImg(Usr.ircp.nick) + "images/" + Usr.ircp.nick + ".jpg";
			FlexGlobals.topLevelApplication.actualizarFoto(true);
			ExternalInterface.call(Config.eventEmitterFuncion, "fotos", "Añadir foto");
		}
		


		
		public static function quitarFoto():void {
			var loader:URLLoader = new URLLoader();
			var request:URLRequest = new URLRequest(Config.serverSQL + "/a/nofoto.php?nombre="+ Usr.ircp.nick);
			loader.load(request);
			Media.urlFoto = "";
			FlexGlobals.topLevelApplication.actualizarFoto(false);
			ExternalInterface.call(Config.eventEmitterFuncion, "fotos", "Quitar foto");
		}
				
		
		public static function iniciarPub():void {
			Config.usarSQL = true;
			Media.webcamOn = true;
			FlexGlobals.topLevelApplication.estadoCam(true);
			ExternalInterface.call(Config.eventEmitterFuncion, "webcam", "Iniciar webcam");
		}
	
		public static function finalizarPub():void {
			Media.webcamOn = false;
			Media.mediaSala = null;
			FlexGlobals.topLevelApplication.estadoCam(false);
			detenerPub();
			ExternalInterface.call(Config.eventEmitterFuncion, "webcam", "Quitar webcam");
			
		}
		
		public static function detenerPub():void {
			var request:URLRequest = new URLRequest(Config.serverSQL + "/a/webcam2.php?e=off&n="+Usr.ircp.nick);
			var loader:URLLoader =new URLLoader();
			loader.load(request);			
		}
					
		
		//Funcion que se ejecuta cuando termina la descarga.
		public static function completarUsuarios(event:Event):void {
			var pos:int;
			var cargador:URLLoader = event.target as URLLoader;
			XmlUsuarios = new XML(cargador.data);
			Usuarios.usuariosMedia.clear();
			var s1:String = XmlUsuarios.f;
			var s2:String = XmlUsuarios.c;
			var s3:String = XmlUsuarios.a;
			var s4:String = XmlUsuarios.sc;
			
			
			var arIm:Array = s1.split(",");
			var arCm:Array = s2.split(",");
			var arAm:Array = s3.split(",");
			var arSc:Array = s4.split(",");
			for each(var s1:String in arIm) addUsuario(s1,true,false);
			for each(var s2:String in arCm) {
				if((pos = s2.indexOf(";")) != -1) {
					addUsuario(s2.substr(0,pos),false,true, parseInt(s2.substr(pos +1)));	
				}else addUsuario(s2,false,true);	
			}
			for each(var s3:String in arAm) {
				if((pos = s3.indexOf(";")) != -1) {
					addUsuario(s3.substr(0,pos),false,true, parseInt(s3.substr(pos +1)));	
				}else addUsuario(s3,true,true);
			}
			Media.salasCam.clear();
			for each(var s4:String in arSc) {
				if(s4 != "") {
					if((pos = s4.indexOf(";")) != -1) {
						Media.salasCam.put(s4.substr(0,pos),parseInt(s4.substr(pos +1)));
					}else Media.salasCam.put(s4,0);
				}
			}
			
			FlexGlobals.topLevelApplication.actualizarMedia();
		}		
		
		public static function usrAleatorio():String {
			var ar:Array = Usuarios.usuariosMedia.getValues();
			return (ar[new int(Math.random() * ar.length)]).nick;
		}
		
		public static function denunciarUsr(nick:String):void {
			var code:String = MD5.hash(nick + "ffchatea11");
			var loader:URLLoader = new URLLoader();
			var request:URLRequest = new URLRequest(Config.serverSQL + "/a/denuncia.php?n="+nick);
			loader.load(request);
			Media.bloqueos.put(nick,nick);
			Media.actualizarDenunciaCache();
			if(Media.salasCam.containsKey(nick))Usr.ircp.WriteRawMessage("PART #cam_"+nick+"!"+Media.salasCam.getValue(nick));
			FlexGlobals.topLevelApplication.tabs.selectedChild.ponerMedia();
			
		}
		
		public static function actualizarDenunciaCache():void {
			var la:String = "";
			for each(var a:String in Media.bloqueos.getValues()) {
				la += " " + a;
			}
			Usr.cache.data.bloqueados = la;
			Usr.cache.flush();
		}
		public static function cargarCacheDenunciados():void {
			if (Usr.cache.data.bloqueados != null) {
				
				var s:String = Usr.cache.data.bloqueados;
				var arr:Array = s.split(" ");
				for each(var a:String in arr) {
					Media.bloqueos.put(a,a);
				}
			}
		}
		
		
		public static function viendoWebcams():Boolean {
			return (Media.usuariosCam.size() != 0);
		}
		
		public static function tieneFoto(s:String):Boolean {
			var a:UsrNick = Usuarios.usuariosMedia.getValue(s) as UsrNick;
			if(a == null) return false;
			else return a.foto;
		}
		
		public static function tieneWebcam(s:String):Boolean {
			var a:UsrNick = Usuarios.usuariosMedia.getValue(s) as UsrNick;
			if(a == null) return false;
			else return a.webcam;
		}
		
		public static function tieneSalaCam(s:String):Boolean {
			return Media.salasCam.containsKey(s);
		}
		
		/*
		0-> off
		1-> on
		2-> warn
		3-> block
		4-> SalaCam
		*/
		public static function estadoWebcam(s:String):int {
			var a:UsrNick = Usuarios.usuariosMedia.getValue(s) as UsrNick;
			if(Media.salasCam.containsKey(s)) {
				if(Media.bloqueos.containsKey(s)) return 3;
				else return 4;
			}
			if(a.tieneWebcam()) {
				
				if(Media.bloqueos.containsKey(s)) return 3;
				if(a.votos >7) return 3;
				else if(a.votos >3) return 2;
				else return 1;
			}else return 0;
		}
		
		public static function resolverWebcam(nick:String):Object {			
			if(Config.verProtegida) return null;

			switch (Media.estadoWebcam(nick)) {
				case 0: return null;
				case 1: return Icons.webcam;
				case 2: return Icons.webcamWarn; // "../data/iconos/webcamonwarning.png";
				case 3: return Icons.webcamBlock; // "../data/iconos/webcamonwarning.png";
				case 4: return Icons.salacam;
			}
			return null;
		}	
		
		public static function resolverServidor(s:String):String {
			return Config.webcamServer + s;
		}

		public static function resolverImg(s:String):String {
			return "http://f1.chateagratis.net/";
		}		
		
		public static function resolverFoto(nick:String,nick2:String):Object {
			if(tieneFoto(nick)) {
				if(!Config.verMiniaturas) return Icons.usrFoto;
				else return Media.resolverServidorF(nick)  + 'images/s_'+nick+'.jpg'; 
			}else {
				return devolverIcon(nick2);
			} 
			return Icons.usrDef;
		}

		public static function resolverServidorF(s:String):String {
			return 'http://f1.chateagratis.net/';
		}	
		
		public static function resolverFotoUrl(s:String):String {
			return 'http://f1.chateagratis.net/images/m_'+s+'.jpg';
		}
		
		
		public static function addUsuario(s:String,imagen1:Boolean,cam2:Boolean,votos:int = 0):void {
			Usuarios.usuariosMedia.put(s,new UsrNick(s,imagen1,cam2, false, votos));
		}
		
		
		public static function crearCam(s:String):void {
			
			Media.iniciarSQL();
			Media.usadaWebcam = true;
			if(Media.salasCam.containsKey(s)) {
				
				FlexGlobals.topLevelApplication.abrirSalaCam("#cam_"+s+"!"+Media.salasCam.getValue(s));
			} else {
				//Cam normal
				
				FlexGlobals.topLevelApplication.iniciarCam(s);

			}
		
		}
		
		public static function cerrarCams():void {
			Media.numCams = 0;
			var arr:Array = Media.usuariosCam.getValues();
			for each(var s:String in arr) {
				Media.cerrrarCam(s);
			}
			FlexGlobals.topLevelApplication.camContainer.width = 0;
			FlexGlobals.topLevelApplication.derContainer.width = 0;
		}
		
		public static function cerrrarCam(s:String):void {
				Media.numCams--;
				Media.usuariosCam.remove(s);
				var d:DisplayObject = FlexGlobals.topLevelApplication.camContainer.getChildByName(s);
				var m:Object = (FlexGlobals.topLevelApplication.listaModulos.getValue(s) as Modulo);
				m.modulo.desconectar();
				FlexGlobals.topLevelApplication.listaModulos.remove(s);
				
				FlexGlobals.topLevelApplication.camContainer.removeElement(d);
				if(Media.numCams == 0) {
					FlexGlobals.topLevelApplication.camContainer.width = 0;
					FlexGlobals.topLevelApplication.derContainer.width = 0;
				}
		}	

		
		public static function devolverIcon(s:String):Object {
			switch(s.charAt(0)) {
				case "@": return Icons.usrOp;
				case "+": return Icons.usrDef;
				default: return Icons.usrDef;
			}
			return  Icons.usrDef;
		}
		
	}
}