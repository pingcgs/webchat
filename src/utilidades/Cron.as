package utilidades
{
	import Config;
	
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import mx.core.FlexGlobals;
	import flash.utils.Timer;
	
	import mx.controls.Alert;
	import flash.system.System;
	
	import org.osmf.events.TimeEvent;
	
	public class Cron
	{
		private var myTime:Timer; 
		public static var min:int = 0;
		
		public function Cron()
		{
			
			myTime = new Timer(1000 * 60);
			myTime.addEventListener(TimerEvent.TIMER, timer1Min);
			myTime.start();
			
			myTime.dispatchEvent(new TimerEvent(TimerEvent.TIMER));	
			Media.CargarXMLWebcam();
		}
		
		
		public function timer1Min(event:TimerEvent):void {
			Cron.min++;
				
			if(Cron.min % 2 == 0) timerLoad();
			
			if(Cron.min % 2 == 0) timerLog();
			
			if((Cron.min % 5 == 0) && !Media.usadaWebcam) {
				Media.CargarXMLWebcam();
				
			} 
			
			if(Cron.min == 2 || Cron.min == 1){
				//FlexGlobals.topLevelApplication.moduloPop('Publicidad','notif','Publicidad');
				//FlexGlobals.topLevelApplication.moduloPop('Publicidad','notificadorSuperior','Publicidad');
			} 
			
			
		}
		
		public function timerLoad():void {
			System.gc();
			if(Media.usadaWebcam) Media.CargarXMLWebcam();
		}
		
		public function timerLog():void {
			if(Config.usarSQL) {
				var loader:URLLoader = new URLLoader();
				var request:URLRequest = new URLRequest(Config.serverSQL + "/a/addLogOnline.php?p="+Usr.ircp.nick+"&i="+Config.ident);
				loader.load(request);
			}
		}
		
	}
}