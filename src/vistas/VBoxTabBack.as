package vistas
{
	import flash.display.DisplayObject;
	
	import modulos.ModuloRoulette;
	
	import mx.containers.Panel;
	import mx.containers.VBox;
	import mx.modules.Module;

	public class VBoxTabBack extends VBox
	{
		public var chanId:String = "Borja";
		private var panel:Panel;
		
		public function VBoxTabBack(id:String)
		{
			super();
			
			panel = new Panel();
			panel.percentHeight = 100;
			panel.percentWidth = 100;
			panel.styleName = "txtPanel";
			this.addChild(panel);
			chanId = "Tab " + id;
		}
		
		override public function addChild(child:DisplayObject):DisplayObject {
			panel.addChild(child);
			return panel;
		}
		
		
		public function isPresent(s:String):Boolean { return false; }
		
		public function write(s:String):void {
			//(getChildAt(0) as ModuloRoulette).write(s);
		}
		
	}
}