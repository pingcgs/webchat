package vistas
{
	import mx.controls.Alert;
	
	import utilidades.Icons;
	
	public class Textos
	{
		
		public static const COLORS:Array = ["FFFFFF", "000000", "00007f", "009300", "ff0000", "7f0000", "9c009c", "fc7f00", "ffff00", "00fc00", "009393", "00ffff", "0000fc", "ff00ff", "7f7f7f", "d2d2d2", "ebc758"];

		
		private static function charToRgb(param1:String) : String
		{
			return "#" + COLORS[parseInt(param1)];
		}			
		
		private static function getColor() : String
		{
			return "<font color=\"" + charToRgb(arguments[1]) + "\"" + (arguments[3].length > 0 ? (" face=\"" + charToRgb(arguments[3]) + "\"") : ("")) + ">" + arguments[4] + "</font>";
		}
		
		private static function urlReplace() : String
		{
			
			var arg:String = arguments[0];
			if (arg.indexOf("http") != 0)
			{
				arg = "" + arg;
			} 

			
			if(Config.urlSearch  != "" && arg.indexOf(Config.urlSearch) != -1) {
				arg = "http://" +Config.urlReplace;
				arguments[0] = Config.urlReplace;
			}
			return "<a href=\"" + arg + "\" target=\"_blank\">" + arguments[0] + "</a>";
		}		
		
		public static function htmlCode(s:String):String {
			if(s.charAt(0) == "*") s = "\x0303" + s;
			if(s.charAt(0) == "%") s = "\x0304" + s;
			
			s = s.replace(/\</g, "&lt;").replace(/\>/g, "&gt;");
			s = s.replace(/(?i)\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))/g, urlReplace);
			s = s.replace(new RegExp("\\x0F", "ig"), "");
			//s = s.replace(/(#[\w-\.]+)/g, "\x0312<a href=\"event:join->$1\">$1</a>\x03");
			s = s.replace(new RegExp("\\x03(\\d{1,2})([,](\\d{1,2}))?([^\\x03]+)", "ig"), getColor);
			s = s.replace(new RegExp("\\x02([^\\x02]+)\\x02?", "ig"), "<b>$1</b>");
			s = s.replace(new RegExp("\\x1F([^\\x1F]+)\\x1F?", "ig"), "<u>$1</u>");
			//s = s.replace(new RegExp("[mod](.*)[/mod]", "ig"), "<a href='http://www.chateagratis.net'>$1</a>");
			s = s.replace(new RegExp("\\x03", "ig"), "");
			//s = "<font size=\""+Config.tamLetra+"\">"+s+"</font>";
			return s;
		}
		
		public static function StringReplaceAll( source:String, find:String, replacement:String ) : String
		{
			return source.split( find ).join( replacement );
		}
		
		public static function emoteCode(s:String):String {
			for(var i:int = 0; i<  Icons.smilies.length;i++) {
				s = Textos.StringReplaceAll(s,Icons.smilies[i], "<img src=\"../data/img/" + Icons.smiliesGfx[i] + ".png\" height=\"19\">");
			} 
			return s;
		}
		
		public static function limpiarHtml(s:String):String {
			s = s.replace(new RegExp("\\x0F", "ig"), "$1");
			s = s.replace(new RegExp("\\x03(\\d{1,2})([,](\\d{1,2}))?([^\\x03]+)", "ig"), "$1");
			s = s.replace(new RegExp("\\x02([^\\x02]+)\\x02?", "ig"), "$1");
			s = s.replace(new RegExp("\\x1F([^\\x1F]+)\\x1F?", "ig"), "$1");
			s = s.replace(new RegExp("\\x03", "ig"), "");
			return s;
		}
		
	}
}