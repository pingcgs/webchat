package 
{

	import Config;
	
	import contenedores.UsrNick;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;

	public class Amigos
	{
		
		public static var listaAmigos:ArrayCollection = new ArrayCollection();	
		
		public function Amigos()
		{
		}
		
		public static function cargarAmigos():void {
			var cmdAmigos = "";
			if (Usr.cache.data.amigos == null ) cmdAmigos = "";
			else {
				cmdAmigos = String(Usr.cache.data.amigos);
			}
			if(cmdAmigos != "") Usr.ircp.WriteRawMessage("WATCH "+ cmdAmigos + "");
		}
		
		
		/*********************************************************************
		 * 							AMIGOS
		 * *******************************************************************/
		
		public static function actualizarCacheAmigos():void {
			var la:String = "";
			for each(var a:UsrNick in Amigos.listaAmigos) {
				la += "+" + a.mNick + " ";
			}
			Usr.cache.data.amigos = la;
			Usr.cache.flush();
		}
		
		public static function addAmigo(nick:String,conectado:Boolean):void {
			var n:UsrNick = Amigos.getNick(nick);
			if( n == null) {
				var a:UsrNick = new UsrNick(nick);
				a.conectado = conectado;	
				listaAmigos.addItem(a);
			} else {
				n.conectado = conectado;
				listaAmigos.refresh();
			}
			actualizarCacheAmigos();
		}

		public static function desconectarAmigo(nick:String):void {
			var n:UsrNick = Amigos.getNick(nick);
			if( n != null) {
				n.conectado = false;
				listaAmigos.refresh();
			}
		}
				
		
		public static function eliminarAmigo(nick:String):void {
			var aNick:UsrNick = getNick(nick);
			var i:int = listaAmigos.getItemIndex(aNick);
			if(i!=-1) {
				listaAmigos.removeItemAt(i);
				Usr.ircp.WriteRawMessage("WATCH -"+nick);
			} 
			actualizarCacheAmigos();
		}

		public static function esAmigo(nick:String):Boolean {
			var newNick:UsrNick = 	new UsrNick(nick);
			if(listaAmigos != null) {
				for each(var aNick:UsrNick in listaAmigos) {
					if(aNick.mNick == newNick.mNick) return true;
				}
			}
			return false;
		}		
		
		public static function getNick(nick:String):UsrNick {
			var newNick:UsrNick = 	new UsrNick(nick);
			if(listaAmigos != null) {
				for each(var aNick:UsrNick in listaAmigos) {
					if(aNick.mNick == newNick.mNick) return aNick;
				}
			}
			return null;
		}
		
	}
}