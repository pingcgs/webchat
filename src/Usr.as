package
{
	import contenedores.HashMap;
	
	import events.QueryEvent;
	
	import flash.net.SharedObject;
	
	import irc.IRCColors;
	import irc.IRCProtocol;
	import irc.IrcNickWatch;
	
	import mx.collections.ArrayCollection;
	import mx.core.FlexGlobals;
	import mx.effects.easing.Back;

	public class Usr
	{
		
		public static var mayor18:Boolean = false;
		public static var ircp:IRCProtocol;
		public static var color:String = "";
		public static var negrita:String = "";
		public static var estado:String = "- Disponible";
		public static var miFoto:String = "";
		public static var camPublicando:Boolean = false;
		public static var miSalaCam:String = "";
		
		public static var bots:Array = new Array("Registra-Me","antispam", "Eggdrop", "Eggdrop1", "Guardian" , "SHaDoW", "Guardian2", "Guardian3" , "Guardian4" , "Guardian5" , "Guardian6", "Guardian7" , "Guardian8" , "Guardian9" , "Guardiana");
		public static var botsSpam:Array = new Array("Registra-Me","antispam");
		
		public static var privados:HashMap = new HashMap();
		public static var salas:HashMap = new HashMap();
		
		public static var sonido:Boolean = true;
		
		public static var cache:SharedObject;
		public static var listaSalas:Array = new Array();
		
		

		public function Usr()
		{
			
			
		}
		
		public static function borrarSalasCache():void {
			Usr.listaSalas = null;
			Usr.cache.data.salas = null;
			Usr.cache.flush();
		}
		
		public static function borrarCache():void {
			Usr.cache.clear();
			Usr.listaSalas = null;
		}
		
		
		public static function estaSala(s:String):Boolean {
			for each(var s2:String in Usr.listaSalas){
				if(s.toLowerCase()==s2.toLocaleLowerCase()) return true;
			}
			return false;
		}
		
		public static function addSala(s:String):void {
			if(!Usr.estaSala(s) && s != "" && s != null) {
				Usr.listaSalas.push(s);
				if(Usr.listaSalas.length>15) {
					
					Usr.listaSalas.pop();
				}
			}
			actualizarCacheSalas();
		}
		
		public static function cargarCacheSalas():void {
			if(Usr.cache.data.salas != null) {
				Usr.listaSalas = Usr.cache.data.salas.split(",");
			}	
		}
		
		public static function actualizarCacheSalas():void {
			var la:String = "";
			for each(var a:String in Usr.listaSalas) {
				la += "" + a + ",";
			}
			la = la.substr(0,la.length -1);
			Usr.cache.data.salas = la;
			Usr.cache.flush();
		}
		
		public static function servidorFoto(nick:String):String {
			return "http://f1.chateagratis.net/";
		}
		
		public static function esIdentChatea(s:String):Boolean {
			if(s.substring(0,3)== "fl_") return true;
			else return false;
		}
		
		public static function ponerColor(col:int):void {
			color = "\x03" + IRCColors.colors[col].value;
		}	
		
		public static function abrirPrivado(s:String):void {
			if(s.charAt(0) == "@" || s.charAt(0) == "+") s = s.substr(1);
			if(s.charAt(0) == "@" || s.charAt(0) == "+") s = s.substr(1);
			
			var qe:QueryEvent = new QueryEvent(s);
			qe.mostrar = true;
			FlexGlobals.topLevelApplication.dispatchEvent(qe);
		}
		
		public static function estaBloqueado(c:String):Boolean {
			return (Usr.botsSpam.indexOf(c) != -1);
		}
		
		public static function desBloquear(c:String):void {		
			Usr.botsSpam.splice(Usr.botsSpam.indexOf(c),1);
		}
		
		public static function bloquear(c:String):void {
			Usr.botsSpam.push(c);
		}
		
		

		

		
	}
}
