package com.facebook.commands.events {
	
	import com.facebook.facebook_internal;
	import com.facebook.net.FacebookCall;
	import com.facebook.utils.FacebookDataUtils;
	
	use namespace facebook_internal;
	
	/**
	 * The Invite class represents the public
	 Facebook API known as Events.invite.
	 * @see http://wiki.developers.facebook.com/index.php/Events.invite
	 */
	public class Invite extends FacebookCall {
		
		
		public static const METHOD_NAME:String = 'events.invite';
		public static const SCHEMA:Array = ['eid', 'uids', 'personal_message'];
		
		public var eid:String;
		public var uids:Array;
		public var personal_message:String;
		
		public function Invite(eid:String, uids:Array, personal_message:String = null) {
			super(METHOD_NAME);
			
			this.eid = eid;
			this.uids = uids;
			this.personal_message = personal_message;
		}
		
		override facebook_internal function initialize():void {
			applySchema(SCHEMA, eid, FacebookDataUtils.toArrayString(uids), personal_message);
			super.facebook_internal::initialize();
		}
	}
}